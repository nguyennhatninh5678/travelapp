import * as admin from 'firebase-admin';
import serviceAccount from '../../travel-app-fac13-firebase-adminsdk-lq9gp-9a9469034d.json';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as admin.ServiceAccount)
});

export default admin;