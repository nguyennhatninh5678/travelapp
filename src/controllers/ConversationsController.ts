import { Request, Response, NextFunction } from 'express';
import { Service } from 'typedi';
import { Controller, Post, Get, Middleware, Put } from '@overnightjs/core';
import { BaseResponse } from '../services/BaseResponse';
import { UserService } from '../services';
import { ConversationsService } from '../services/ConversationService';
import { MessagesService } from '../services/MessagesService';
import { User, Conversations, Messages } from '../entities';
import { JwtInfo } from '../models/JwtInfo';
import { MessagesReq } from '../models/MessagesReq';
import { ConversationGroupReq } from '../models/ConversationGroupReq';
import { UpdateConversationReq } from '../models/UpdateConversationReq';
import { checkJwt } from '../middleware/checkJwt.middleware';
import { uploadMiddleware } from '../middleware/upload.middleware';
import { MessageStatus } from '../consts/MessageStatus';
import path from 'path';

@Service()
@Controller('api/conversations')
export class ConversationsController {
  private className = 'ConversationsController';
  constructor(
    private readonly userService: UserService,
    private readonly conversationsService: ConversationsService,
    private readonly messagesService: MessagesService
  ) {}
  private dataResponse: BaseResponse = new BaseResponse();

  @Post('')
  @Middleware([checkJwt, uploadMiddleware('file', 10)])
  private async addConversationWithMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      const messageReq = req.body as MessagesReq;
      const members = [jwtInfo.uuid, Number(messageReq.uuidUser)];
      if (req.body.content || req.file) {
        const user: User = await this.userService.findById(jwtInfo.uuid).catch((e) => {
          throw e;
        });
        const otherUser: User = await this.userService.findById(messageReq.uuidUser).catch((e) => {
          throw e;
        });
        const today = new Date();
        const message = new Messages();
        message.author = user;
        message.time = today;
        req.body.content?.trim() && (message.content = req.body.content);
        const fileMessage = `${process.env.UPLOAD_FOLDER}/${req.file?.filename}`;
        req.file?.filename && (message.file = fileMessage.toString());

        const newConversations = new Conversations();
        newConversations.members = members;
        newConversations.title = `${user.usr}, ${otherUser.usr}`;
        newConversations.image = `${user.avatar}, ${otherUser.avatar}`;
        req.body.content && (newConversations.content = `${user.usr}: ${req.body.content}`);
        req.file && !req.body.content && (newConversations.content = `${user.usr}: sent a file`);
        await newConversations.save();
        message.conversation = newConversations;

        await message.save();
        this.dataResponse.status = 200;
        this.dataResponse.data = { ...message, time: `${today.getHours()}:${today.getMinutes()}`, received: members };
        this.dataResponse.message = 'Add Conversations Successfull';
        res.status(200).json(this.dataResponse.data);
      }
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Post('messages')
  @Middleware([checkJwt, uploadMiddleware('file', 10)])
  private async addMessagesInConversation(req: Request, res: Response, next: NextFunction): Promise<void> {
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      const messageReq = req.body as MessagesReq;
      if (req.body.content || req.file) {
        const user: User = await this.userService.findById(jwtInfo.uuid).catch((e) => {
          throw e;
        });
        const conversations: Conversations = await this.conversationsService.findById(messageReq.uuidConversation).catch((e) => {
          throw e;
        });
        const today = new Date();
        const message = new Messages();
        message.author = user;
        message.time = today;
        req.body.content?.trim() && (message.content = req.body.content);
        const fileMessage = `${process.env.UPLOAD_FOLDER}/${req.file?.filename}`;
        req.file?.filename && (message.file = fileMessage.toString());
        req.body.content && (conversations.content = `${user.usr}: ${req.body.content}`);
        req.file && !req.body.content && (conversations.content = `${user.usr}: sent a file`);
        await conversations.save();
        message.conversation = conversations;
        await message.save();
        this.dataResponse.status = 200;
        this.dataResponse.data = { ...message, time: `${today.getHours()}:${today.getMinutes()}`, received: conversations.members };
        this.dataResponse.message = 'Add message Successfull';
        res.status(200).json(this.dataResponse.data);
      }
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Post('group')
  @Middleware([checkJwt])
  private async addConversationGroup(req: Request, res: Response, next: NextFunction): Promise<void> {
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      const conversationGroupReq: ConversationGroupReq = req.body;
      let members: number[] = [jwtInfo.uuid, ...conversationGroupReq.uuidUser];

      const conversation: Conversations = await this.conversationsService.findById(conversationGroupReq?.uuidConversation).catch((e) => {
        throw e;
      });

      req.body.uuidConversation && (members = [...conversation.members, ...conversationGroupReq.uuidUser]);

      const users: User[] = await this.userService.findWithIdUsers(members).catch((e) => {
        throw e;
      });
      const lastNameOtherUser = users.slice(1).reduce((acc, user) => (acc += `, ${user.lastName}`), '');
      const avatarOtherUser = users.slice(1).reduce((acc, user) => (acc += `, ${user.avatar}`), '');

      const conversations = new Conversations();
      req.body.uuidConversation ? (conversations.admin = conversation.members) : (conversations.admin = members);
      conversations.members = members;
      conversations.title = ` ${users[0].lastName}${lastNameOtherUser}`;
      conversations.image = `${users[0].avatar}${avatarOtherUser}`;
      conversations.content = `${jwtInfo.usr}: created a group`;
      await conversations.save();
      this.dataResponse.status = 200;
      this.dataResponse.data = conversations;
      this.dataResponse.message = 'Add Conversations group Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Put('group')
  @Middleware([checkJwt, uploadMiddleware('image', 10)])
  private async updateConversationGroup(req: Request, res: Response, next: NextFunction): Promise<void> {
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      const updateGroupReq = req.body as UpdateConversationReq;
      const conversation: Conversations = await this.conversationsService.findById(updateGroupReq.uuidConversation).catch((e) => {
        throw e;
      });
      const isAdmin = conversation.admin.includes(jwtInfo.uuid);
      if (isAdmin && updateGroupReq.uuidUserDelete) {
        const indexUserDelete = conversation.members.indexOf(updateGroupReq.uuidUserDelete);
        conversation.members.splice(indexUserDelete, 1);
      }
      updateGroupReq.uuidUser && (conversation.members = [...conversation.members, ...updateGroupReq.uuidUser]);
      const users: User[] = await this.userService.findWithIdUsers(updateGroupReq.uuidUser).catch((e) => {
        throw e;
      });
      const lastNameUserJoin = users.slice(1).reduce((acc, user) => (acc += `, ${user.lastName}`), '');
      const avatarUserJoin = users.slice(1).reduce((acc, user) => (acc += `, ${user.avatar}`), '');
      conversation.titleDefaut && (conversation.title += lastNameUserJoin);
      conversation.imageDefaut && (conversation.image += avatarUserJoin);

      updateGroupReq.title && (conversation.title = updateGroupReq.title) && (conversation.titleDefaut = false);

      const image = `${process.env.UPLOAD_FOLDER}/${req.file?.filename}`;
      req.file?.filename && (conversation.image = image.toString()) && (conversation.imageDefaut = false);
      await conversation.save();
      this.dataResponse.status = 200;
      this.dataResponse.data = conversation;
      this.dataResponse.message = 'Update Conversation group Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Get('')
  @Middleware([checkJwt])
  private async getConversationsOfUser(req: Request, res: Response, next: NextFunction): Promise<void> {
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    const id = jwtInfo.uuid;
    try {
      const data: Conversations[] = await this.conversationsService.getConversationInUser(id).catch((e) => {
        throw e;
      });
      const result = data.map((conversation) => {
        const titleArr = conversation.title.split(', ');
        const imageArr = conversation.image.split(', ');
        const indexUser = conversation.members.indexOf(id);
        titleArr.splice(indexUser, 1);
        imageArr.splice(indexUser, 1);
        const isAuthor = conversation.content.split(': ')[0] === jwtInfo.usr;
        return {
          ...conversation,
          title: conversation.titleDefaut ? titleArr : conversation.title,
          image: conversation.imageDefaut ? imageArr : conversation.image,
          content: isAuthor ? conversation.content.replace(jwtInfo.usr, 'You') : conversation.content
        };
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Get Conversations of User Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Put('')
  @Middleware([checkJwt])
  private async updateConversations(req: Request, res: Response, next: NextFunction): Promise<void> {
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    const id = req.body.uuidConversation;
    try {
      const data: Conversations = await this.conversationsService.findById(id).catch((e) => {
        throw e;
      });

      const titleConversations = data.title.split(', ');
      const indexUser = data.members.indexOf(jwtInfo.uuid);
      const newtitle = indexUser === 0 ? `${titleConversations[0]}, ${req.body.title}` : ` ${req.body.title}, ${titleConversations[1]}`;
      req.body.title && (data.title = newtitle);
      await data.save();
      this.dataResponse.status = 200;
      this.dataResponse.data = data;
      this.dataResponse.message = 'Update Conversations Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Get('messages/:id')
  @Middleware([checkJwt])
  private async getMessagesFromConversation(req: Request, res: Response, next: NextFunction): Promise<void> {
    const id = req.params.id;
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      const message: Messages = await this.messagesService.getLastMessageInConversation(Number(id)).catch((e) => {
        throw e;
      });
      const conversation: Conversations = await this.conversationsService.findById(id, { relations: ['messages', 'messages.author'] }).catch((e) => {
        throw e;
      });

      message.author?.uuid !== jwtInfo.uuid && (message.status = MessageStatus.Seen);
      await message.save();
      console.log(message);
      console.log(conversation);

      await conversation.save();
      const result = conversation.messages.map((message) => {
        const timeFormat = `${message.time.getHours()}:${message.time.getMinutes()}`;
        const nameAuthor = message.author.usr;
        if (message.author.uuid === jwtInfo.uuid) {
          return { ...message, messageOfMe: true, author: nameAuthor, time: timeFormat };
        } else {
          return { ...message, messageOfMe: false, author: nameAuthor, time: timeFormat };
        }
      });

      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Get massage in Conversations Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Put('messages')
  private async updateMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
    const id = req.body.uuidMessage;
    const status = req.body?.status.online as boolean;

    console.log(req.body);
    try {
      const messages: Messages = await this.messagesService.findById(id).catch((e) => {
        throw e;
      });
      status && (messages.status = MessageStatus.Received);
      req.body?.content && (messages.content = req.body.content);
      await messages.save();
      console.log(messages);

      this.dataResponse.status = 200;
      this.dataResponse.data = messages;
      this.dataResponse.message = 'Update Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Get('members/:id')
  private async getUserInConversation(req: Request, res: Response, next: NextFunction): Promise<void> {
    const id = req.params.id;
    try {
      const conversation: Conversations = await this.conversationsService.findById(Number(id)).catch((e) => {
        throw e;
      });
      const members = conversation.members;
      const users: User[] = await this.userService.findWithIdUsers(members).catch((e) => {
        throw e;
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = users;
      this.dataResponse.message = 'Get user in Conversation Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Get('users/:id')
  private async getUser(req: Request, res: Response, next: NextFunction): Promise<void> {
    const id = req.params.id;
    try {
      const conversation: Conversations = await this.conversationsService.findById(id).catch((e) => {
        throw e;
      });
      const members = conversation.members;
      const users: User[] = await this.userService.findOtherUser(members).catch((e) => {
        throw e;
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = users;
      this.dataResponse.message = 'Get user Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Get('message/test')
  private async test(req: Request, res: Response, next: NextFunction): Promise<void> {
    const indexPath = path.join(__dirname, '..', 'index.html');
    await res.sendFile(indexPath);
  }
}
