import { Request, Response, NextFunction } from 'express';
import { Service } from 'typedi';
import { Controller, Post, Get, Middleware } from '@overnightjs/core';
import { BaseResponse } from '../services/BaseResponse';
import { UserService, DestinationService } from '../services';
import { BookedReq } from '../models/BookedReq';
import { User, Destination, Booked } from '../entities';
import { JwtInfo } from '../models/JwtInfo';
import { checkJwt } from '../middleware/checkJwt.middleware';
import path from 'path';

@Service()
@Controller('api/booked')
export class BookedController {
  private className = 'BookedController';
  constructor(private readonly userService: UserService, private readonly destinationService: DestinationService) {}
  private dataResponse: BaseResponse = new BaseResponse();

  @Post('add')
  @Middleware([checkJwt])
  private async addBooked(req: Request, res: Response, next: NextFunction): Promise<void> {
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      const data = req.body as BookedReq;
      const destination: Destination = await this.destinationService.findById(data.uuidDestination).catch((e) => {
        throw e;
      });
      const user: User = await this.userService.findById(jwtInfo.uuid).catch((e) => {
        throw e;
      });
      const today = new Date();
      const booked = new Booked();
      booked.totalPeople = data.totalPeople;
      booked.date = today;
      booked.user = user;
      booked.destination = destination;
      await booked.save();
      const result = {
        ...booked,
        user: user.usr,
        destination: destination.name,
        price: destination.priceTicket,
        total: booked.totalPeople * destination.priceTicket
      };
      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Booked Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
}
