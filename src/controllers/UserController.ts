import { NextFunction, Request, Response } from 'express';
import { Controller, Middleware, Get, Put, Post } from '@overnightjs/core';
import { checkJwt } from '../middleware/checkJwt.middleware';
import { checkRole } from '../middleware/checkRole.middleware';
import { UserService } from '../services/UserService';
import { User } from '../entities/User';
import { Roles } from '../consts/Roles';
import { Service } from 'typedi';
import Log from '../utils/Log';
import { uploadMiddleware } from '../middleware/upload.middleware';
import { BaseResponse } from '../services/BaseResponse';
import { JwtInfo } from 'src/models/JwtInfo';
import nodemailer from 'nodemailer';
import * as dotenv from 'dotenv';

dotenv.config();

const option = {
  service: 'gmail',
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAIL_PASSWORD
  }
};

const transporter = nodemailer.createTransport(option);

@Service()
@Controller('api/user')
export class UserController {
  private dataResponse: BaseResponse = new BaseResponse();
  private className = 'UserController';
  constructor(private readonly userService: UserService) {}

  @Get('list')
  @Middleware([checkJwt, checkRole([{ role: Roles.CUSTOMER }])])
  private async listUser(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'listUser', `RQ`, { req: req });

    try {
      const result: User[] = await this.userService.getCustomerUsers().catch((e) => {
        throw e;
      });

      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Get('userId/:id')
  @Middleware([checkJwt, checkRole([{ role: Roles.CUSTOMER }])])
  private async getUserById(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'getUser', `RQ`, { req: req });

    try {
      const id: string = req.params.id;
      const item: User = await this.userService.findById(id).catch((e) => {
        throw e;
      });

      this.dataResponse.status = 200;
      this.dataResponse.data = item;
      this.dataResponse.message = 'Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Get('me')
  @Middleware([checkJwt, checkRole([{ role: Roles.CUSTOMER }])])
  private async getProfile(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'user', `RQ`, { req: req });
    console.log(res.locals.jwtPayload);
    try {
      const item: User = await this.userService.findById(res.locals.jwtPayload['uuid']).catch((e) => {
        throw e;
      });

      this.dataResponse.status = 200;
      this.dataResponse.data = item;
      this.dataResponse.message = 'Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Post('register')
  private async addUser(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'addUser', `RQ`, { req: req });
    console.log(req.body);

    try {
      const user: User = <User>req.body;
      user.phone.replace(' ', '');
      const getName = user.usr.split(' ');
      user.lastName = getName.pop();
      user.firstName = getName.join(' ');

      if (!user.pwd && user.pwd.trim() === '') {
        this.dataResponse.status = 400;
        this.dataResponse.data = {};
        this.dataResponse.message = 'Invalid password';
        res.status(400).json(this.dataResponse);
        return;
      }

      let result: User;
      try {
        result =
          (await this.userService.findByUserName(user.usr)) ||
          (await this.userService.findByEmail(user.email)) ||
          (await this.userService.findByPhone(user.phone));
      } catch (error) {
        console.log(error);
        return next(error);
      }

      if (result) {
        const message =
          result.usr === user.usr ? 'Username already exists' : result.phone === user.phone ? 'Phone already exists' : 'Email already exists';
        this.dataResponse.status = 400;
        this.dataResponse.data = {};
        this.dataResponse.message = message;
        res.status(400).json(this.dataResponse);
        return;
      }

      const code = Math.floor(1000 + Math.random() * 9000);

      user.code = code.toString();

      const newUser: User = await this.userService.store(user).catch((e) => {
        throw e;
      });

      try {
        transporter.verify(function (err: any, success: any) {
          if (err) {
            console.log(err);
          } else {
            console.log('Connected successfully');
            const mail = {
              from: process.env.EMAIL,
              to: newUser.email.toString(),
              subject: 'Verify your email address',
              html: teamplateVerfification(newUser.lastName, newUser.code)
            };

            transporter.sendMail(mail, function (err: any, info: any) {
              if (err) {
                console.log(err);
              } else {
                console.log('Mail sent: ' + info.response);
              }
            });
          }
        });
      } catch (e) {
        console.error(e);
      }

      this.dataResponse.status = 200;
      this.dataResponse.data = newUser;
      this.dataResponse.message = 'Register Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Post('code')
  private async checkCode(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'addUser', `RQ`, { req: req });

    try {
      const result: User = await this.userService.findByEmail(req.body.email).catch((e) => {
        throw e;
      });

      if (result) {
        this.dataResponse.data = {};
        if (result.code !== req.body.code) {
          this.dataResponse.status = 400;
          this.dataResponse.message = ' Code not math';
          res.status(400).json(this.dataResponse);
        } else {
          this.dataResponse.status = 200;
          result.verifyCode = true;
          await result.save();
          this.dataResponse.message = 'Code Successfull';
          res.status(200).json(this.dataResponse);
        }

        return;
      } else {
        this.dataResponse.message = ' User already exists ';
      }

      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Code Successfull';

      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Post('resend_code')
  private async resendCode(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'addUser', `RQ`, { req: req });

    try {
      const result: User = await this.userService.findByEmail(req.body.email).catch((e) => {
        throw e;
      });

      if (result != null) {
        const codeRan = Math.floor(1000 + Math.random() * 9000);
        result.code = codeRan.toString();
        await result.save();

        this.dataResponse.status = 200;

        if (result.email !== null) {
          try {
            transporter.verify(function (err: any, success: any) {
              if (err) {
                console.log(err);
              } else {
                console.log('Connected successfully');
                const mail = {
                  from: process.env.EMAIL,
                  to: result.email.toString(),
                  subject: 'Verify your email address',
                  html: teamplateVerfification(result.lastName, result.code)
                };

                transporter.sendMail(mail, function (err: any, info: any) {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log('Mail sent: ' + info.response);
                  }
                });
              }
            });
          } catch (e) {
            console.error(e);
          }
        }
        this.dataResponse.data = {
          code: result.code
        };
        this.dataResponse.message = 'Successfull';
        res.status(200).json(this.dataResponse);
        return;
      } else {
        this.dataResponse.message = ' User not exists ';
      }

      this.dataResponse.status = 400;
      this.dataResponse.data = result;
      res.status(400).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Post('change_password')
  @Middleware([checkJwt, checkRole([{ role: Roles.CUSTOMER }])])
  private async changePassword(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'updateUser', `RQ`, { req: req });
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      console.log(req.body.oldPass);
      console.log(req.body.newPass);
      console.log(req.body.confirmPass);
      console.log(jwtInfo);

      const user: User | undefined = await this.userService.findById(res.locals.jwtPayload['uuid']).catch((err) => {
        throw err;
      });

      if (req.body.oldPass == user.pwd) {
        user.pwd = req.body.newPass;
        this.dataResponse.status = 200;
        this.dataResponse.data = user;
        this.dataResponse.message = 'Update the successfull';
        await user.save();
        res.status(200).json(this.dataResponse);
      } else {
        this.dataResponse.status = 400;
        this.dataResponse.data = {};
        this.dataResponse.error = 101;
        this.dataResponse.message = 'Incorrect the old Password';
        res.status(200).json(this.dataResponse);
      }
    } catch (e) {
      next(e);
    }
  }

  @Put('edit_profile')
  @Middleware([checkJwt, uploadMiddleware('avatar', 10), checkRole([{ role: Roles.CUSTOMER }])])
  private async updateUser(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'updateUser', `RQ`, { req: req });

    try {
      const user: User = <User>req.body;
      console.log(user);
      const newUser: User = await this.userService.findById(res.locals.jwtPayload['uuid']).catch((e) => {
        throw e;
      });
      user.firstName && (newUser.firstName = user.firstName) && (newUser.usr = `${user.firstName} ${newUser.lastName}`);
      user.lastName && (newUser.lastName = user.lastName) && (newUser.usr = `${newUser.firstName} ${user.lastName}`);
      user.phone && (newUser.phone = user.phone);
      user.location && (newUser.location = user.location);
      user.birthdate && (newUser.birthdate = user.birthdate);
      const avatar = `${process.env.UPLOAD_FOLDER}/${req.file?.filename}`;
      req.file?.filename && (newUser.avatar = avatar.toString());
      await newUser.save();

      this.dataResponse.status = 200;
      this.dataResponse.data = newUser;
      this.dataResponse.message = 'Update the successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Get('my_schedule')
  @Middleware([checkJwt])
  private async mySchedule(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'updateUser', `RQ`, { req: req });

    try {
      const newUser: User = await this.userService
        .findById(res.locals.jwtPayload['uuid'], { relations: ['booked', 'booked.destination'] })
        .catch((e) => {
          throw e;
        });
      const result = newUser.booked.map((booked) => booked.destination);
      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }
}

export default function teamplateVerfification(fullName: string, code: string): string {
  return ` <div style="width: 100% ; padding-bottom: 20px; padding-top: 10px;text-align: center; border-top: 4px solid #3773d7 ; background-color: #fafafa; color: #000;">
    <img src="https://drive.google.com/uc?id=1v3TYX7ayqyMtAYIrBOQqVwngO1oHn8cl" alt=""/>
    <div style="background-color: #fff; padding: 30px;margin:0 8% 26px; font-size: 12px;">
      <h3 style="padding-bottom: 18px; font-size: 18px;">Help us protect your account</h3>
      Hi ${fullName}! Before you finish creating new account, we need to verify your identity. On the verification page, enter the following code.
      <div style="padding: 8px 0; margin: 20px 35%; background-color: #ddd; font-size: 20px; font-weight: 600;">
        ${code}
      </div>
      If you have any questions, send us an email [serveruits@gmail.com to your support team].
      We’re glad you’re here!
    </div>
  </div>`;
}
