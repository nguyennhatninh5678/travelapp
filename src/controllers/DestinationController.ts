import { Request, Response, NextFunction } from 'express';
import { Service } from 'typedi';
import { Controller, Post, Get } from '@overnightjs/core';
import { BaseResponse } from '../services/BaseResponse';
import { Destination } from '../entities/Destination';
import { DestinationService } from '../services/DestinationService';

@Service()
@Controller('api/destination')
export class DestinationController {
  private className = 'DestinationController';
  constructor(private readonly destinationService: DestinationService) {}
  private dataResponse: BaseResponse = new BaseResponse();

  @Post('add')
  private async addDestination(req: Request, res: Response, next: NextFunction): Promise<void> {
    console.log(req.body);

    try {
      const input = <Destination>req.body;
      const data = await this.destinationService.store(input).catch((e) => {
        throw e;
      });

      this.dataResponse.status = 200;
      this.dataResponse.data = data;
      this.dataResponse.message = 'Add Destination Successfull';

      res.status(200).json(this.dataResponse);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
  @Get('')
  private async getDestination(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data: Destination[] = await this.destinationService.index().catch((e) => {
        throw e;
      });
      const result = data.map((destination) => {
        return {
          id: destination.uuid,
          name: destination.name,
          address: destination.address,
          image: JSON.parse(destination.images)[0],
          rate: destination.rate
        };
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Get Destinations Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }

  @Get('search')
  private async searchDestination(req: Request, res: Response, next: NextFunction): Promise<void> {
    const searchData = req.query.value as string;

    try {
      const data: Destination[] = await this.destinationService.searchDestination(searchData).catch((e) => {
        throw e;
      });

      this.dataResponse.status = 200;
      this.dataResponse.data = data;
      this.dataResponse.message = 'Search Destination Successfull';

      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }

  @Get(':id')
  private async getDestinationId(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data: Destination = await this.destinationService.findById(req.params.id).catch((e) => {
        throw e;
      });
      const arrImages = JSON.parse(data.images);
      const result = {
        ...data,
        images: arrImages
      };

      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Get Destination Successfull';

      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
}
