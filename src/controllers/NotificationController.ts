import { NextFunction, Request, Response } from 'express';
import { Controller, Middleware, Get, Put, Post, Delete } from '@overnightjs/core';
import { checkJwt } from '../middleware/checkJwt.middleware';
import { NotificationService } from '../services/NotificationService';
import { UserService } from '../services/UserService';
import { Notification } from '../entities/Notification';
import { User } from '../entities/User';
import { Service } from 'typedi';
import Log from '../utils/Log';
import { BaseResponse } from '../services/BaseResponse';
import { NotificationMessage } from 'src/models/NotificationMessage';

@Service()
@Controller('api/notification')
export class NotificationController {
  private dataResponse: BaseResponse = new BaseResponse();
  private className = 'CommentController';
  constructor(private readonly notifcationService: NotificationService, private readonly userService: UserService) {}

  @Get('list')
  @Middleware([checkJwt])
  private async listNotification(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'listUser', `RQ`, { req: req });
    try {
      const data: User = await this.userService.findById(res.locals.jwtPayload['uuid'], { relations: ['notification'] }).catch((e) => {
        throw e;
      });
      const notifications: Notification[] = data.notification;
      this.dataResponse.status = 200;
      this.dataResponse.data = notifications;
      this.dataResponse.message = 'Get Destinations Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }

  @Post('')
  @Middleware([checkJwt])
  private async postNotification(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'listUser', `RQ`, { req: req });
    const idUser = res.locals.jwtPayload['uuid'];
    const contentNotification = req.body as NotificationMessage;
    try {
      await this.notifcationService.sendNotification(contentNotification).catch((e) => {
        throw e;
      });
      const notification = {
        title: contentNotification.title,
        body: contentNotification.body,
        author: idUser,
        type: 1
      };
      const data: Notification = await this.notifcationService.store(notification).catch((e) => {
        throw e;
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = data;
      this.dataResponse.message = 'Post Notifacation Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }

  @Post('topic')
  @Middleware([checkJwt])
  private async postNotificationTopic(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'listUser', `RQ`, { req: req });
    const idUser = res.locals.jwtPayload['uuid'];
    const contentNotification = req.body as NotificationMessage;
    try {
      await this.notifcationService.sendNotificationTopic(contentNotification).catch((e) => {
        throw e;
      });
      const notification = {
        title: contentNotification.title,
        body: contentNotification.body,
        author: idUser,
        type: 2
      };
      const data: Notification = await this.notifcationService.store(notification).catch((e) => {
        throw e;
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = data;
      this.dataResponse.message = 'Post Notifacation Successfull';
      res.status(200).json(this.dataResponse.data);
    } catch (e) {
      console.log(e);
      next(e);
    }
  }
}
