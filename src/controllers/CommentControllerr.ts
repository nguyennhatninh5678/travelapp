import { NextFunction, Request, Response } from 'express';
import { Controller, Middleware, Get, Put, Post, Delete } from '@overnightjs/core';
import { checkJwt } from '../middleware/checkJwt.middleware';
import { checkRole } from '../middleware/checkRole.middleware';
import { UserService } from '../services/UserService';
import { DestinationService } from '../services/DestinationService';
import { User } from '../entities/User';
import { Roles } from '../consts/Roles';
import { Service } from 'typedi';
import Log from '../utils/Log';
import { BaseResponse } from '../services/BaseResponse';
import { JwtInfo } from 'src/models/JwtInfo';
import { CommentReq } from '../models/CommentReq';
import { Comment } from '../entities/Comment';
import { Destination } from '../entities/Destination';

@Service()
@Controller('api/comment')
export class CommentController {
  private dataResponse: BaseResponse = new BaseResponse();
  private className = 'CommentController';
  constructor(private readonly userService: UserService, private readonly destinationService: DestinationService) {}

  @Get('destination/:id')
  private async listCommentDestinationId(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'listUser', `RQ`, { req: req });

    try {
      const id: string = req.params.id;
      const data: Destination = await this.destinationService.findById(id, { relations: ['comments', 'comments.author'] }).catch((e) => {
        throw e;
      });
      const comments = data.comments;
      const result = comments.map((comment) => {
        return {
          ...comment,
          author: comment.author.usr
        };
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Get Comment of Destination Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Get('')
  @Middleware([checkJwt, checkRole([{ role: Roles.CUSTOMER }])])
  private async getCommentUserId(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'getUser', `RQ`, { req: req });
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    try {
      const id = jwtInfo.uuid;
      const data: User = await this.userService
        .findById(id, { relations: ['comments', 'comments.destination'], order: { updatedAt: 'DESC' } })
        .catch((e) => {
          throw e;
        });
      const result = data.comments.map((comment) => {
        return {
          ...comment,
          destination: comment.destination.name
        };
      });
      this.dataResponse.status = 200;
      this.dataResponse.data = result;
      this.dataResponse.message = 'Get Comment of Destination Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }

  @Post('add')
  @Middleware([checkJwt])
  private async addComment(req: Request, res: Response, next: NextFunction): Promise<void> {
    Log.info(this.className, 'addComment', `RQ`, { req: req });
    const jwtInfo = <JwtInfo>res.locals.jwtPayload;
    console.log(jwtInfo);
    try {
      const comment: CommentReq = req.body as CommentReq;
      const destination: Destination = await this.destinationService.findById(comment.uuidDestination, { relations: ['comments'] }).catch((e) => {
        throw e;
      });
      const user: User = await this.userService.findById(jwtInfo.uuid).catch((e) => {
        throw e;
      });
      console.log(destination.comments);

      const totalRate = destination.comments.reduce((sum, cur) => sum + Number(cur.rate), 0);
      destination.rate = (totalRate + comment.rate) / (destination.comments.length + 1);
      await destination.save();

      const cm = new Comment();
      cm.comment = comment.comment;
      cm.rate = comment.rate;
      cm.author = user;
      cm.destination = destination;
      await cm.save();

      this.dataResponse.status = 200;
      this.dataResponse.data = cm;
      this.dataResponse.message = 'Add Comment Successfull';
      res.status(200).json(this.dataResponse);
    } catch (e) {
      next(e);
    }
  }
}
