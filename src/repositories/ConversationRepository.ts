import { EntityRepository, Repository } from 'typeorm';
import { Conversations } from '../entities/Conversations';
import { Service } from 'typedi';


@Service()
@EntityRepository(Conversations)
export class ConversationRepository extends Repository<Conversations> {
  getConversationInUser(userId: number): Promise<Conversations[]> {
    return this.createQueryBuilder('conversations')
    .where("JSON_CONTAINS(conversations.members, :userId)", { userId: `[${userId}]` })
    .getMany();
  }
}
