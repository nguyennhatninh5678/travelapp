import { EntityRepository, Repository, UpdateResult } from 'typeorm';
import { User } from '../entities/User';
import { Roles } from '../consts/Roles';
import { Service } from 'typedi';

@Service()
@EntityRepository(User)
export class UserRepository extends Repository<User> {
  getCustomerUsers(): Promise<User[]> {
    return this.createQueryBuilder().where('role = :role', { role: Roles.CUSTOMER }).getMany();
  }

  getByUsername(usr: string): Promise<User> {
    return this.createQueryBuilder().where('usr = :usr', { usr: usr }).getOne();
  }

  getByEmail(email: string): Promise<User> {
    return this.createQueryBuilder().where('email = :email', { email: email }).getOne();
  }

  getByPhone(phone: string): Promise<User> {
    return this.createQueryBuilder().where('phone = :phone', { phone: phone }).getOne();
  }

  getUserInConversation(conversationId: number): Promise<User[]> {
    return this.createQueryBuilder('users')
      .innerJoin('users.messages', 'messages')
      .where('messages.conversationUuid = :conversationId', { conversationId: conversationId })
      .getMany();
  }

  getOtherUser(ids: number[]): Promise<User[]> {
    return this.createQueryBuilder('users').where('users.uuid NOT IN (:...ids)', { ids: ids }).getMany();
  }

  updateNewPassword(email: string, pwd: string): Promise<UpdateResult> {
    return this.createQueryBuilder()
      .update(User)
      .set({
        pwd: pwd
      })
      .where('email = :email', { email: email })
      .execute();
  }

  updateCodeUser(usr: string, code: string): Promise<UpdateResult> {
    return this.createQueryBuilder()
      .update(User)
      .set({
        code: code
      })
      .where('usr = :usr', { usr: usr })
      .execute();
  }

  updateUser(usr: string, firstName: string, lastName: string, phone: string): Promise<UpdateResult> {
    return this.createQueryBuilder()
      .update(User)
      .set({
        firstName: firstName,
        lastName: lastName,
        phone: phone
      })
      .where('usr = :usr', { usr: usr })
      .execute();
  }
}
