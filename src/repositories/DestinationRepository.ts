import { EntityRepository, Repository } from 'typeorm';
import { Destination } from '../entities/Destination';
import { Service } from 'typedi';

@Service()
@EntityRepository(Destination)
export class DestinationRepository extends Repository<Destination> {
    searchDestination(name: string): Promise<Destination[]> {
        return this.createQueryBuilder().where("name LIKE :name", { name: `%${name}%` }).getMany();
      }
}
