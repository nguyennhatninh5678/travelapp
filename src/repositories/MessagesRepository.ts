import { EntityRepository, Repository } from 'typeorm';
import { Messages } from '../entities/Messages';
import { Service } from 'typedi';

@Service()
@EntityRepository(Messages)
export class MessagesRepository extends Repository<Messages> {
  getLastMessageInConversation(conversationId: number): Promise<Messages> {
    return this.createQueryBuilder('messages')
      .where('messages.conversationUuid = :conversationId', { conversationId: conversationId })
      .orderBy('messages.createdAt', 'DESC')
      .getOne();
  }
}
