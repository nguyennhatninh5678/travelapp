import { EntityRepository, Repository } from 'typeorm';
import { Booked } from '../entities/Booked';
import { Service } from 'typedi';

@Service()
@EntityRepository(Booked)
export class BookedRepository extends Repository<Booked> {}