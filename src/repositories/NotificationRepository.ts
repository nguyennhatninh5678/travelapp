import { EntityRepository, Repository } from 'typeorm';
import { Notification } from '../entities/Notification';
import { Service } from 'typedi';

@Service()
@EntityRepository(Notification)
export class NotificationRepository extends Repository<Notification> {}
