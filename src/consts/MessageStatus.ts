export enum MessageStatus {
  Sent = "sent",
  Received = "received",
  Seen = "seen"
}
