export const enum Roles {
  NONE = 0,
  ADMIN = 1,
  CUSTOMER = 2,
  GUEST = 3
}
