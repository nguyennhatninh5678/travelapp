import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, BaseEntity, ManyToOne, ManyToMany } from 'typeorm';
import { MessageStatus } from '../consts/MessageStatus';
import { User } from './User';
import { Conversations } from './Conversations';

@Entity('messages')
export class Messages extends BaseEntity {
  @PrimaryGeneratedColumn()
  uuid: number;

  @Column({
    type: 'text',
    nullable: true
  })
  content: string;

  @Column({
    name: 'file',
    nullable: true
  })
  file: string;

  @Column()
  time: Date;

  @Column({
    type: "enum",
    enum: MessageStatus,
    default: MessageStatus.Sent
  })
  status: MessageStatus;

  @ManyToOne(() => User, (user) => user.messages)
  author: User;

  @ManyToOne(() => Conversations, (conversation) => conversation.messages)
  conversation: Conversations;

  @CreateDateColumn({
    name: 'created_at',
    nullable: false
  })
  createdAt: string;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: false
  })
  updatedAt: string;
}
