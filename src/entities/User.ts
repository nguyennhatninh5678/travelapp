import { Roles } from '../consts/Roles';
import { BaseEntity, Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Exclude } from 'class-transformer';
import { Comment } from './Comment';
import { Booked } from './Booked';
import { Notification } from './Notification';
import { Messages } from './Messages';

@Entity({ name: 'users' })
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  uuid: number;

  @Column({
    length: 150
  })
  usr: string;

  @Exclude()
  @Column({
    nullable: true,
    length: 150
  })
  pwd: string;

  @Column('text', {
    name: 'first_name',
    nullable: true
  })
  firstName: string;

  @Column('text', {
    name: 'last_name',
    nullable: true
  })
  lastName: string;

  @Column({
    name: 'email',
    nullable: false
  })
  email: string;

  @Column({
    name: 'phone',
    nullable: true
  })
  phone: string;

  @Column('text', {
    name: 'id_login',
    nullable: true
  })
  idLogin: string;

  @Column({
    nullable: true
  })
  role: Roles;

  @Column({
    name: 'device_token',
    nullable: true
  })
  device_token: string;

  @Column({
    name: 'avatar',
    nullable: true
  })
  avatar: string;

  @Column({
    name: 'code',
    length: 4,
    nullable: true
  })
  code: string;

  @Column({
    name: 'verify',
    nullable: true,
    default: false
  })
  verifyCode: boolean;

  @Column({
    name: 'location',
    nullable: true
  })
  location: string;

  @Column({
    name: 'birthdate',
    nullable: true
  })
  birthdate: string;

  @OneToMany(() => Comment, (comment) => comment.author)
  comments: Comment[];

  @OneToMany(() => Booked, (booked) => booked.user)
  booked: Booked[];

  @OneToMany(() => Notification, (notifi) => notifi.author)
  notification: Notification[];

  @OneToMany(() => Messages, (message) => message.author)
  messages: Messages[];

  @CreateDateColumn({
    name: 'created_at',
    nullable: false
  })
  createdAt: string;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: false
  })
  updatedAt: string;

  constructor(partial: Partial<User>) {
    super();
    Object.assign(this, partial);
  }
}
