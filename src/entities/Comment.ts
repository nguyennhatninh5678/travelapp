import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, BaseEntity, ManyToOne } from 'typeorm';
import { User } from './User';
import { Destination } from './Destination';

@Entity('comments')
export class Comment extends BaseEntity {
  @PrimaryGeneratedColumn()
  uuid: number;

  @Column()
  rate: number;

  @Column()
  comment: string;

  @ManyToOne(() => User, (user) => user.comments)
  author: User;

  @ManyToOne(() => Destination, (destination) => destination.comments)
  destination: Destination;

  @CreateDateColumn({
    name: 'created_at',
    nullable: false
  })
  createdAt: string;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: false
  })
  updatedAt: string;
}
