import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, BaseEntity, ManyToOne } from 'typeorm';
import { User } from './User';
import { Destination } from './Destination';


@Entity('booked')
export class Booked extends BaseEntity {
  @PrimaryGeneratedColumn()
  uuid: number;

  @Column()
  totalPeople: number;

  @Column()
  date: Date;

  @ManyToOne(() => User, (user) => user.booked)
  user: User;

  @ManyToOne(() => Destination, (destination) => destination.booked)
  destination: Destination;

  @CreateDateColumn({
    name: 'created_at',
    nullable: false
  })
  createdAt: string;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: false
  })
  updatedAt: string;
}
