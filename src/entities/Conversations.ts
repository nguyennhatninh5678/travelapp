import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, BaseEntity, ManyToMany, OneToMany, JoinTable } from 'typeorm';
import { User } from './User';
import { Messages } from './Messages';

@Entity('conversations')
export class Conversations extends BaseEntity {
  @PrimaryGeneratedColumn()
  uuid: number;

  @Column({
    type: 'text'
  })
  content: string;

  @Column({
    type: 'text'
  })
  title: string;

  @Column({
    name: 'image',
    nullable: true
  })
  image: string;

  @Column({
    default: true
  })
  titleDefaut: boolean;

  @Column({
    default: true
  })
  imageDefaut: boolean;

  @Column({
    type: 'json',
    nullable: true
  })
  members: number[];

  @Column({
    type: 'json',
    nullable: true
  })
  admin: number[];

  @OneToMany(() => Messages, (message) => message.conversation)
  messages: Messages[];

  @CreateDateColumn({
    name: 'created_at',
    nullable: false
  })
  createdAt: string;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: false
  })
  updatedAt: string;
}
