import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, BaseEntity, ManyToOne } from 'typeorm';
import { User } from './User';

@Entity('notification')
export class Notification extends BaseEntity {
  @PrimaryGeneratedColumn()
  uuid: number;

  @Column()
  type: number;

  @ManyToOne(() => User, (user) => user.notification)
  author: User;

  @Column('text')
  title: string;

  @Column('text')
  body: string;

  @CreateDateColumn({
    name: 'created_at',
    nullable: false
  })
  createdAt: string;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: false
  })
  updatedAt: string;
}
