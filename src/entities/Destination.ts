import { BaseEntity, Column, CreateDateColumn, Double, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Comment } from './Comment';
import { Booked } from './Booked';

@Entity({ name: 'destination' })
export class Destination extends BaseEntity {
  @PrimaryGeneratedColumn()
  uuid: number;

  @Column({
    name: 'name',
    nullable: false
  })
  name: string;

  @Column({
    name: 'address',
    nullable: false
  })
  address: string;

  @Column({
    name: 'description',
    nullable: false,
    type: 'text'
  })
  description: string;

  @Column({
    name: 'phone',
    nullable: true
  })
  phone: string;

  @Column({
    name: 'images',
    nullable: true,
    type: 'text'
  })
  images: string;

  @Column({
    type: 'decimal',
    precision: 5,
    scale: 1,
    nullable: true
  })
  rate: number;

  @Column()
  priceTicket: number;

  @OneToMany(() => Comment, (comment) => comment.destination)
  comments: Comment[];

  @OneToMany(() => Booked, (booked) => booked.destination)
  booked: Booked[];

  @CreateDateColumn({
    name: 'created_at',
    nullable: false
  })
  createdAt: string;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: false
  })
  updatedAt: string;

  constructor(partial: Partial<Destination>) {
    super();
    Object.assign(this, partial);
  }
}
