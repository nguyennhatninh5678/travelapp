import * as controllers from './controllers';
import { Server as HTTPServer } from 'http';
import { Server } from '@overnightjs/core';
import Log from './utils/Log';
import { Request, Response, NextFunction } from 'express';
import { errorHandler } from './middleware/error.middleware';
import { notFoundHandler } from './middleware/notFound.middleware';
import 'reflect-metadata';
import Container from 'typedi';
import { createConnection, useContainer } from 'typeorm';
import * as entities from './entities/index';
import * as ormconfig from '../ormconfig';
import express from 'express';
import { Server as ioServer } from 'socket.io';

class ApiServer extends Server {
  private className = 'ApiServer';
  private appserver: HTTPServer;
  private io: ioServer;

  private options = {
    dotfiles: 'ignore',
    etag: false,
    extensions: ['jpg', 'png', 'gif'],
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res: any, path: any, stat: any) {
      res.set('x-timestamp', Date.now());
    }
  };

  constructor() {
    super(true);
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.all('/*', this.setupCORS);
    this.app.use(express.static('public', this.options));
    this.app.use(process.env.UPLOAD_FOLDER, express.static('upload'));
  }

  private async initServer(): Promise<void> {
    useContainer(Container);

    // eslint-disable-next-line
    const arrEntities: any[] = [];
    for (const name in entities) {
      if (Object.prototype.hasOwnProperty.call(entities, name)) {
        // eslint-disable-next-line
        const entity: any = (entities as any)[name];
        arrEntities.push(entity);
      }
    }
    // eslint-disable-next-line
    const configDB: any = {
      ...ormconfig.default,
      entities: arrEntities,
      migrations: [],
      cli: undefined
    };

    await createConnection(configDB).catch((ex) => {
      if (ex.name === 'AlreadyHasActiveConnectionError') {
        console.log('The connection is already open');
      } else {
        console.log(`** createDBConnection error:`, `config: ${JSON.stringify(configDB)} - name:${ex.name} - msg:${ex.message}`);
      }
    });
    this.setupControllers();
    this.app.use(errorHandler);
    this.app.use(notFoundHandler);
  }

  private setupControllers(): void {
    const ctlrInstances = [];
    for (const name in controllers) {
      if (Object.prototype.hasOwnProperty.call(controllers, name)) {
        // eslint-disable-next-line
        const controller = Container.get((controllers as any)[name]);
        ctlrInstances.push(controller);
      }
    }
    super.addControllers(ctlrInstances);
  }
  private setupSocketEvents(statusUser: { [key: string]: { online: boolean } }): void {
    this.io.on('connection', (socket) => {
      const userId = socket.handshake.query.userId as string;
      statusUser[userId] = { online: true };
      console.log('A user connected');
      console.log(statusUser);
      socket.on('message-sent', (data) => {
        this.io.emit('return-message', data);
      });
      socket.on('disconnect', () => {
        console.log('User disconnected');
        statusUser[userId].online = false;
      });
      
      socket.on('message-status', (data) => {
        const getStatusUserReceived = data.received.map((idUser: number) => {
          return { [idUser]: statusUser[String(idUser)]};
        })
        const dataResponse = { status: getStatusUserReceived, idMessage: data.idMessage  }
        console.log(dataResponse);
        socket.emit('status-response', dataResponse);
      });


    });
  }

  public async start(port: number): Promise<void> {
    const funcName = 'start';

    try {
      await this.initServer();

      this.appserver = this.app.listen(port, () => {
        Log.info(this.className, funcName, `Server started on port: ${port}`);
      });
      this.io = new ioServer(this.appserver);
      const statusUser: { [key: string]: { online: boolean } } = {};
      this.setupSocketEvents(statusUser);
      this.appserver.setTimeout(parseInt(<string>process.env.SERVER_TIMEOUT, 10));
    } catch (ex) {
      Log.info(this.className, funcName, ex);
    }
  }

  public stop(): void {
    this.appserver.close();
  }

  private setupCORS(req: Request, res: Response, next: NextFunction): void {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.setHeader('Access-Control-Allow-Credentials', 'true');

    res.header('Access-Control-Allow-Headers', `Origin, X-Requested-With, Content-type, Accept, X-Access-Token, X-Key, Authorization`);

    const allowOrigins: string[] = (<string>process.env.ALLOW_ORIGIN).split(',');
    let origin = '';
    const headersOrigin = req.headers.origin ? <string>req.headers.origin : '';

    if (allowOrigins.length === 1 && allowOrigins[0] === '*') origin = headersOrigin;
    else if (allowOrigins.indexOf(headersOrigin.toLowerCase()) > -1) origin = headersOrigin;
    else origin = allowOrigins[0];

    res.header('Access-Control-Allow-Origin', origin);

    if (req.method === 'OPTIONS') {
      res.status(200).end();
    } else {
      next();
    }
  }
}

export default ApiServer;
