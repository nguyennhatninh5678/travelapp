import { Destination } from '../entities/Destination';
import { DestinationRepository } from '../repositories/DestinationRepository';
import { BaseService } from './BaseService';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class DestinationService extends BaseService<Destination, DestinationRepository> {
  constructor(@InjectRepository(Destination) repository: DestinationRepository) {
    super(repository);
  }
  async searchDestination(name: string): Promise<Destination[]> {
    const data = await this.repository.searchDestination(name);
    const filteredDestinations = data.filter((destination) => destination.name.includes(name));
    return filteredDestinations;
  }
}
