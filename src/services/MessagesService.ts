import { Messages } from '../entities/Messages';
import { MessagesRepository } from '../repositories/MessagesRepository';
import { BaseService } from './BaseService';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class MessagesService extends BaseService<Messages, MessagesRepository> {
  constructor(@InjectRepository(Messages) repository: MessagesRepository) {
    super(repository);
  }
  getLastMessageInConversation(conversationId: number): Promise<Messages>{
    return this.repository.getLastMessageInConversation(conversationId)
  }
}
