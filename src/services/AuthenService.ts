import { User } from '../entities/User';
import { AuthenReq } from '../models/AuthenReq';
import { SocialReq } from '../models/SocialReq';
import { UserInfoGoogle } from '../models/UserInfoGoogle';
import { UserRepository } from '../repositories/UserRepository';
import { AuthenRes } from '../models/AuthenRes';
import { UserInfoFacebook } from '../models/UserInfoFacebook';
import * as jwt from 'jsonwebtoken';
import { JwtInfo } from '../models/JwtInfo';
import AppException from '../exceptions/AppException';
import { BaseService } from './BaseService';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { UserRes } from 'src/models/UserRes';
import { CodeRes } from 'src/models/CodeRes';
import { NewPasswordReq } from 'src/models/NewPasswordReq';
import { BaseResponse } from './BaseResponse';
import nodemailer from 'nodemailer';
import axios from 'axios';
import * as dotenv from 'dotenv';

dotenv.config();

const option = {
  service: 'gmail',
  auth: {
    user: process.env.EMAIL,
    pass: process.env.EMAIL_PASSWORD
  }
};

const transporter = nodemailer.createTransport(option);

@Service()
export class AuthenService extends BaseService<User, UserRepository> {
  constructor(@InjectRepository(User) repository: UserRepository) {
    super(repository);
  }
  private dataResponse: BaseResponse = new BaseResponse();

  public async login(authenReq: AuthenReq): Promise<AuthenRes | undefined> {
    if (!authenReq.idLogin && (!authenReq.usr || authenReq.usr.trim().length === 0 || !authenReq.pwd || authenReq.pwd.trim().length === 0)) {
      throw new AppException('login_failed', 'usr or pwd empty');
    }

    let user: User;
    try {
      user =
        (await this.repository.getByUsername(authenReq.usr)) ||
        (await this.repository.getByEmail(authenReq.usr)) ||
        (await this.repository.getByPhone(authenReq.usr.replace(' ', '')));
    } catch (error) {
      console.log(error);
    }

    if (!authenReq.idLogin && user.verifyCode === false) {
      console.log('verifyCode false');
      return null;
    }

    if (user && (user.pwd === authenReq.pwd || authenReq.idLogin === user.idLogin)) {
      const jwtInfo: JwtInfo = {
        uuid: user.uuid,
        usr: user.usr,
        role: user.role
      };

      const token = jwt.sign(jwtInfo, <string>process.env.JWT_SECRET, { expiresIn: process.env.TOKEN_EXPIRE });

      const userResData: UserRes = {
        id: user.uuid,
        usr: user.usr,
        firstName: user.firstName,
        lastName: user.lastName,
        avatar: user.avatar,
        phone: user.phone,
        role: user.role,
        location: user.location,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt
      };

      const authenRes: AuthenRes = {
        user: userResData,
        token: token
      };

      return authenRes;
    } else {
      throw new AppException('login_failed', !user ? 'user doest not exist' : 'wrong password');
    }
  }

  public async getInfoUserFromGoogle(socialReq: SocialReq): Promise<UserInfoGoogle | undefined> {
    try {
      const { data } = await axios.get('https://www.googleapis.com/oauth2/v3/userinfo', {
        params: {
          access_token: socialReq.access_token,
          alt: 'json'
        },
        headers: {
          Authorization: 'Bearer ' + socialReq.id_token
        }
      });
      console.log(data);

      return data as UserInfoGoogle;
    } catch (e) {
      console.log(e);
    }
  }
  public async loginGoogle(socialReq: SocialReq): Promise<AuthenRes | undefined> {
    const userInfo = await this.getInfoUserFromGoogle(socialReq);
    const user: User = await this.repository.findOne({ email: userInfo.email });
    if (user) {
      user.avatar = userInfo.picture;
      await user.save();
      const data = await this.login({ usr: userInfo.email, idLogin: `google_${userInfo.sub}` } as AuthenReq);
      return data;
    } else {
      const infoRegister: User = {
        usr: userInfo.name,
        email: userInfo.email,
        role: 2,
        device_token: socialReq.device_token,
        firstName: userInfo.given_name,
        lastName: userInfo.family_name,
        idLogin: `google_${userInfo.sub}`
      } as User;
      const newUser: User = await this.store(infoRegister).catch((e) => {
        throw e;
      });
      const data = await this.login({ usr: newUser.email, idLogin: `google_${userInfo.sub}` } as AuthenReq);
      return data;
    }
  }
  public async getInfoUserFromFaceBook(socialReq: SocialReq): Promise<UserInfoFacebook | undefined> {
    try {
      const { data } = await axios.get(`https://graph.facebook.com/me?fields=id,name,email,picture&access_token=${socialReq.access_token}`);
      const result = {
        ...data,
        picture: data.picture.data.url
      };
      console.log(result);
      return result;
    } catch (e) {
      console.log(e);
    }
  }

  public async loginFacebook(socialReq: SocialReq): Promise<AuthenRes | undefined> {
    const userInfo = await this.getInfoUserFromFaceBook(socialReq);
    const user: User = await this.repository.findOne({ email: userInfo.email });
    if (user) {
      user.avatar = userInfo.picture;
      await user.save();
      const data = await this.login({ usr: userInfo.email, idLogin: `facebook_${userInfo.id}` } as AuthenReq);
      return data;
    } else if (userInfo.email || userInfo.phone) {
      const getName = userInfo.name.split(' ');
      const infoRegister: User = {
        usr: userInfo.name,
        email: userInfo.email,
        role: 2,
        device_token: socialReq.device_token,
        firstName: getName.pop(),
        lastName: getName.join(' '),
        idLogin: `facebook_${userInfo.id}`
      } as User;
      const newUser: User = await this.store(infoRegister).catch((e) => {
        throw e;
      });
      const data = await this.login({ usr: newUser.email || userInfo.phone, idLogin: `facebook_${userInfo.id}` } as AuthenReq);
      return data;
    }
  }
  public async forgotPass(username: string): Promise<CodeRes | undefined> {
    console.log('start');
    console.log(username);

    if (username.trim().length === 0) {
      throw new AppException('login_failed', 'username  empty');
    }

    console.log('getByUsername');

    const user: User | undefined = await this.repository.getByUsername(username).catch((err) => {
      throw err;
    });

    console.log('user');
    console.log(user);
    if (!user) {
      throw new AppException('The User doest not exist', !user ? 'user doest not exist' : 'wrong password');
    }
    const codeRan = Math.floor(1000 + Math.random() * 9000);
    user.code = codeRan.toString();
    await user.save();

    try {
      transporter.verify(function (err: any, success: any) {
        if (err) {
          console.log(err);
        } else {
          console.log('Connected successfully');
          const mail = {
            from: process.env.EMAIL,
            to: user.email.toString(),
            subject: 'Verify your email address',
            html: teamplate(user.lastName, user.code)
          };

          transporter.sendMail(mail, function (err: any, info: any) {
            if (err) {
              console.log(err);
            } else {
              console.log('Mail sent: ' + info.response);
            }
          });
        }
      });
    } catch (e) {
      console.error(e);
    }

    const code: CodeRes = {
      code: user.code
    };
    return code;
  }

  public async newPass(body: NewPasswordReq): Promise<CodeRes | undefined> {
    if (body.email.trim().length === 0) {
      throw new AppException('login_failed', 'email  empty');
    }

    const user: User | undefined = await this.repository.getByEmail(body.email).catch((err) => {
      throw err;
    });

    if (user) {
      if (body.code.toString() !== user.code.toString()) {
        throw new AppException('Code not math', 'Code not math');
      }

      user.pwd = body.newPass;
      await this.repository.updateNewPassword(body.email, body.newPass).catch((err) => {
        throw err;
      });

      const code: CodeRes = {
        code: '0000'
      };
      return code;
    } else {
      throw new AppException('The User doest not exist', !user ? 'user doest not exist' : 'wrong password');
    }
  }
}

export default function teamplate(fullName: string, code: string): string {
  return ` <div style="width: 100% ; padding-bottom: 20px; padding-top: 10px;text-align: center; border-top: 4px solid #3773d7 ; background-color: #fafafa; color: #000;">
    <img src="https://drive.google.com/uc?id=1v3TYX7ayqyMtAYIrBOQqVwngO1oHn8cl" alt=""/>
    <div style="background-color: #fff; padding: 30px;margin:0 8% 26px; font-size: 12px;">
      <h3 style="padding-bottom: 18px; font-size: 18px;">Help us protect your account</h3>
      Hi ${fullName}! Before you finish creating new password, we need to verify your identity. On the verification page, enter the following code.
      <div style="padding: 8px 0; margin: 20px 35%; background-color: #ddd; font-size: 20px; font-weight: 600;">
        ${code}
      </div>
      If you have any questions, send us an email [serveruits@gmail.com to your support team].
      We’re glad you’re here!
    </div>
  </div>`;
}
