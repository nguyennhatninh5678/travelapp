import { Booked } from '../entities/Booked';
import { BookedRepository } from '../repositories/BookedRepository';
import { BaseService } from './BaseService';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class BookedService extends BaseService<Booked, BookedRepository> {
  constructor(@InjectRepository(Booked) repository: BookedRepository) {
    super(repository);
  }
}