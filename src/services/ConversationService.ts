import { Messages } from '../entities/Messages';
import { Conversations } from '../entities/Conversations';
import { ConversationRepository } from '../repositories/ConversationRepository';
import { BaseService } from './BaseService';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class ConversationsService extends BaseService<Conversations, ConversationRepository> {
  constructor(@InjectRepository(Conversations) repository: ConversationRepository) {
    super(repository);
  }
  getConversationInUser(userId: number): Promise<Conversations[]> {
    return this.repository.getConversationInUser(userId);
  }
}
