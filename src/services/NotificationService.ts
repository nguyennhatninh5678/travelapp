import { NotificationRepository } from '../repositories/NotificationRepository';
import { BaseService } from './BaseService';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Notification } from '../entities/Notification';
import { NotificationMessage } from '../models/NotificationMessage';
import admin from '../firebase/firebaseAdminInit';

@Service()
export class NotificationService extends BaseService<Notification, NotificationRepository> {
  constructor(@InjectRepository(Notification) repository: NotificationRepository) {
    super(repository);
  }
  public async sendNotification(message: NotificationMessage): Promise<void> {
    const payload = {
      notification: {
        title: message.title,
        body: message.body
      },
      token: message.token
    };

    try {
      const response = await admin.messaging().send(payload);
      console.log('Successfully sent message:', response);
    } catch (error) {
      console.error('Error sending message:', error);
      throw new Error('Failed to send message');
    }
  }
  public async sendNotificationTopic(message: NotificationMessage): Promise<void> {
    const payload = {
      notification: {
        title: message.title,
        body: message.body
      },
      topic: message.topic
    };

    try {
      const response = await admin.messaging().send(payload);
      console.log('Successfully sent message:', response);
    } catch (error) {
      console.error('Error sending message:', error);
      throw new Error('Failed to send message');
    }
  }
}
