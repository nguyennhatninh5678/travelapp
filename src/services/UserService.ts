import { User } from '../entities/User';
import { UserRepository } from '../repositories/UserRepository';
import { BaseService } from './BaseService';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export class UserService extends BaseService<User, UserRepository> {
  constructor(@InjectRepository(User) repository: UserRepository) {
    super(repository);
  }

  findByFirstName(firstName: string): Promise<User | null> {
    return this.repository.findOne({ firstName: firstName });
  }

  getCustomerUsers(): Promise<User[]> {
    return this.repository.getCustomerUsers();
  }

  getUserInConversation(conversationId: number): Promise<User[]> {
    return this.repository.getUserInConversation(conversationId);
  }

  findByUserName(username: string): Promise<User | null> {
    return this.repository.findOne({ usr: username });
  }

  findByEmail(email: string): Promise<User | null> {
    return this.repository.findOne({ email: email });
  }

  findByPhone(phone: string): Promise<User | null> {
    return this.repository.findOne({ phone: phone });
  }

  findOtherUser(ids: number[]): Promise<User[]> {
    return this.repository.getOtherUser(ids);
  }

  async findWithIdUsers(ids: number[]): Promise<User[]> {
    const users = await this.repository.findByIds(ids);
    const sortedUsers = ids.map((id) => users.find((user) => id === user.uuid));
    return sortedUsers;
  }
}
