import { UserRes } from './UserRes';

export interface AuthenRes {
  user: UserRes;
  token: string;
}
