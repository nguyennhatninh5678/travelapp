export interface ConversationGroupReq {
  uuidUser: number[];
  uuidConversation?: number;
}
