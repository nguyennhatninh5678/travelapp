import { Roles } from 'src/consts/Roles';

export interface UserRes {
  id: number;
  usr: string;
  firstName: string;
  lastName: string;
  avatar: string;
  phone: string;
  role: Roles;
  location: string;
  createdAt: string;
  updatedAt: string;
}
