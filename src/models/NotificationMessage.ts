export interface NotificationMessage {
  title: string;
  body: string;
  token?: string;
  topic?: string;
}
