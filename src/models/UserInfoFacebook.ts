export interface UserInfoFacebook {
  id: string;
  name: string;
  email: string;
  phone: string;
  picture: string;
}
