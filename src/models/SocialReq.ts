export interface SocialReq {
  access_token: string;
  id_token?: string;
  device_token: string;
}

