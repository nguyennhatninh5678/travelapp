export interface AuthenReq {
  usr: string;
  pwd?: string;
  idLogin?: string;
}
