export interface NewPasswordReq {
    email: string;
    code: string;
    newPass: string;
    confirmPass: string;
  }
  