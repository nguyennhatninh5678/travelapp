export interface MessagesReq {
    uuidUser?: number;
    uuidConversation?: number;
    content: string;
  }