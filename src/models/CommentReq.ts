export interface CommentReq {
  uuidDestination: number;
  rate: number;
  comment: string;
}
