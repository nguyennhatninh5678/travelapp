export interface UpdateConversationReq {
    uuidUser?: number[];
    uuidUserDelete?: number;
    uuidConversation: number;
    title?: string;
    image?: string;
  }