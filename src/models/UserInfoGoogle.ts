export interface UserInfoGoogle {
  sub: string;
  name: string;
  given_name: string;
  picture: string;
  family_name: string;
  email: string;
  email_verified: boolean;
  locale: string;
}
