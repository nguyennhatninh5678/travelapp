export interface BookedReq {
  uuidDestination: number;
  totalPeople: number;
}
